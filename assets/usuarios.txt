MARZO – jueves 11 de marzo Primera Recalada – Cena de Camaradería con Gobernadores Marítimos 2021, Seminario se hizo vía teleconferencia. (Suspendida por COVID 19). 

ABRIL – viernes 30 de abril Primera Recalada Virtual – Reunión de camaradería virtual vía Zoom o Teams. 

MAYO – viernes 28 de mayo Segunda Recalada – Almuerzo de Camaradería a las 13:00 horas en el Club Naval de Valparaíso y homenaje a las Glorias Navales. 

JULIO – viernes 02 de julio Tercera Recalada – Cena Nocturna a las 20:00 horas en lugar por confirmar. 

AGOSTO – viernes 06 de agosto Cuarta Recalada – Misa/Responso en homenaje a los LT caídos en actos de servicio en la Capilla DD.TT. y posterior Elección de Directorio y Asamblea Anual de Socios, a las 10.30 horas, en Auditorio CIMAR (Playa Ancha). Coctel en Quincho DD.TT. 

SEPTIEMBRE – viernes 03 de o lunes 06 de septiembre (por confirmar) Quinta Recalada – 27º Aniversario de la Cofradía. Almuerzo de Camaradería a las 12:30 horas en el Club Naval de Valparaíso. 

OCTUBRE – viernes 15 o viernes 22 de octubre (Por confirmar) Sexta Recalada – Cena bailable con Balandras, a las 20:00 horas en lugar por confirmar. 

NOVIEMBRE – DICIEMBRE – viernes 26 de noviembre o viernes 03 de diciembre (Por confirmar) Séptima Recalada – Almuerzo de fin año por término de actividades a las 13:00 horas en el Quincho de las Direcciones Técnicas. DICIEMBRE Entrega de premios en ESNAVAL, APOLINAV y ACANAV. Entrega de premios en ESGRUM por Subdelegación Marítima de Talcahuano. 

DICIEMBRE - MARZO Cierre de actividades anuales. Receso.		
