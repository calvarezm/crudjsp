package cl.Inacap.FormLogin.Controllers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login.do")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.getRequestDispatcher("site/Login.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		
		Funciones fn=new Funciones();
		
		String email=request.getParameter("nombreUsuario").toString();
		String password=fn.EncriptarMd5(request.getParameter("passUsuario").toString()); 
		
		
		
		//out.println("Mostrar info entra:	"+email+" "+password);
		
		File miArchivo=new File("/Users/carlos.alvarez/eclipse-workspace/FormLogin/assets/login.txt");
		FileReader fr= new FileReader(miArchivo);
		BufferedReader br= new BufferedReader(fr);
			
		String linea;
		Boolean swIngresa=false;
		while((linea=br.readLine())!=null) {
			String [] data=linea.split(";");
			//out.println("Lectura del archivo:"+data[0]+" "+data[1]);
			if(email.equals(data[0]) && password.equals(data[1])) {
				
				HttpSession misession=request.getSession(true);
				misession.setAttribute("SwLogin", true);
				misession.setAttribute("User", email);
				
				swIngresa=true;
				break;
			}
		}
		br.close();
		out.println(swIngresa);
		
	}

}
