package cl.Inacap.FormLogin.Controllers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.Inacap.FormLogin.DAO.PersonaDAO;
import cl.Inacap.FormLogin.DTO.Persona;

/**
 * Servlet implementation class EditPersona
 */
@WebServlet("/EditPersona.do")
public class EditPersona extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditPersona() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PersonaDAO ps= new PersonaDAO();
		
		int Iden=Integer.parseInt(request.getParameter("Iden"));
		
		request.setAttribute("Persona", ps.getPersonabyID(Iden));
		request.setAttribute("IdentificadorPersona", Iden);
		
		request.getRequestDispatcher("site/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String rut=request.getParameter("rut").toString();	
		String fechaNacimiento=request.getParameter("fechaNacimiento").toString();	
		String nombre=request.getParameter("nombre").toString();	
		String EmailPersona=request.getParameter("Email").toString();	
		int index=Integer.parseInt(request.getParameter("index").toString());
		
		Persona p=new Persona();
		p.setNombreApellido(nombre);
		p.setRut(rut);
		p.setEmailPersona(EmailPersona);;
		p.setFechaNacimiento(fechaNacimiento);
		
		PersonaDAO pdao=new PersonaDAO();
		
		pdao.updatePersona(p, index);
		
		
		response.sendRedirect("ListartPersonas.do");
		
		
		
		PrintWriter out=response.getWriter();
		out.println("Servlet de edicar persona");
		
	}

}
