package cl.Inacap.FormLogin.Controllers;

import java.awt.print.Printable;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.Inacap.FormLogin.DAO.PersonaDAO;
import cl.Inacap.FormLogin.DTO.Persona;

/**
 * Servlet implementation class ControlFormulario
 */
@WebServlet("/ControlFormulario.do")
public class ControlFormulario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   

    public ControlFormulario() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.getRequestDispatcher("site/index.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Los datos de los campos del form se obtienen por medio el nombre del campo no del ID 
		
		
		/*PrintWriter out = response.getWriter();
		out.println(Email+ " "+Pass);
		*/
		
		String rut=request.getParameter("rut").toString();	
		String fechaNacimiento=request.getParameter("fechaNacimiento").toString();	
		String nombre=request.getParameter("nombre").toString();	
		String EmailPersona=request.getParameter("Email").toString();	
		String Password=request.getParameter("Password").toString();	
		
		Persona p=new Persona();
		p.setNombreApellido(nombre);
		p.setRut(rut);
		p.setEmailPersona(EmailPersona);;
		p.setFechaNacimiento(fechaNacimiento);
		p.setPassword(Password);
		
		PersonaDAO pdao=new PersonaDAO();
		pdao.AddPersona(p);
		
		
		response.sendRedirect("Home.do");
		
		//new PersonaDAO().AddPersona(p);
			
		
		//Instancia por medio de constructor.
		//Persona p =new Persona(request.getParameter("rut").toString(),request.getParameter("nombre").toString(),request.getParameter("fechaNacimiento").toString(),request.getParameter("emailPersona").toString(),request.getParameter("Password").toString());
		//PersonaDAO pDao=new PersonaDAO();
		//pDao.AddPersona(p);
		
		/*
		 * Instanacia por medio de get and Set
		Persona p=new Persona();
		p.setNombreApellido(request.getParameter("nombre").toString());
		p.setRut(request.getParameter("rut").toString());
		p.setEmail(request.getParameter("Email").toString());
		p.setFechaNacimiento(request.getParameter("fechaNacimiento").toString());
		p.setPassword(request.getParameter("emailPersona").toString());
		
		*/
		
	}

}
