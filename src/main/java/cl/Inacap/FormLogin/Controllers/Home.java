package cl.Inacap.FormLogin.Controllers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import com.sun.xml.ws.runtime.dev.Session;

import cl.Inacap.FormLogin.DAO.PersonaDAO;
import cl.Inacap.FormLogin.DTO.Persona;

/**
 * Servlet implementation class Home
 */
@WebServlet("/Home.do")
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Home() {
        super();
        // TODO Auto-generated constructor stub
    }

	
    
    
    
    
    
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sessionValida=request.getSession(true);
		
		if(sessionValida.getAttribute("SwLogin")==null) {
			request.getRequestDispatcher("site/Login.jsp").forward(request, response);
		}else {
		
			try {
			
				if(sessionValida.getAttribute("cantidadCargas")!=null) {
					int numeroCargas=(int)sessionValida.getAttribute("cantidadCargas");
					sessionValida.setAttribute("cantidadCargas", numeroCargas+1);
					
				}else{
					
					sessionValida.setAttribute("cantidadCargas", 1);
					
					Funciones fn=new Funciones();
					
					for(int i=1; i<=20;i++) {
						Persona p=new Persona();
						p.setNombreApellido("Perona"+i);
						p.setRut("1111111-"+i);
						p.setEmailPersona("Prueba@prueba"+i+".cl");;
						p.setFechaNacimiento("2020-11-"+i);
						p.setPassword(fn.EncriptarMd5("Persona"+i));
						
						PersonaDAO pdao=new PersonaDAO();
						pdao.AddPersona(p);
					}
				}
				
			}catch(Exception e) {
				
			}
			
			
			request.getRequestDispatcher("site/home.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
