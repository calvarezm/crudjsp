package cl.Inacap.FormLogin.Controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.Inacap.FormLogin.DAO.PersonaDAO;
import cl.Inacap.FormLogin.DTO.Persona;


@WebServlet("/ListartPersonas.do")
public class ListartPersonas extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public ListartPersonas() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out=response.getWriter();
		
		PersonaDAO pd=new PersonaDAO();
		List<Persona> personaList=pd.getAllPersonas();		
		
		//personaList.stream().forEach(c->out.println(c.getNombreApellido()+"; Rut: "+c.getRut()));
		
		//out.println("===============");
		//personaList.stream()
		//			.filter(c->c.getRut().equals("1111111-6"))
		//			.forEach(c->out.print(c.getNombreApellido())
		//);
		
		request.setAttribute("ListaPersonas", personaList);
		
		request.getRequestDispatcher("site/ListarPersonas.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Integer Index= Integer.parseInt(request.getParameter("Id").toString());	
		new PersonaDAO().deletePersona(Index);
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		//PrintWriter p=res.;
		
		
	}

}
