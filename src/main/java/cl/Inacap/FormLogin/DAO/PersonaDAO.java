package cl.Inacap.FormLogin.DAO;

import java.util.ArrayList;
import java.util.List;

import cl.Inacap.FormLogin.DTO.Persona;

public class PersonaDAO {
	
	private static List<Persona> arrPersonas= new ArrayList<Persona>();
	
	public void AddPersona(Persona p) {   // Insert
		arrPersonas.add(p);
	}
	
	public List<Persona> getAllPersonas(){   //Select * from 
		return arrPersonas;
	}
	
	public void deletePersona(int id) { 
		arrPersonas.remove(id);
	}
	
	public Persona getPersonabyID(int id) { //
		return arrPersonas.get(id);
	}
	
	
	public void updatePersona(Persona p,int id) {
		arrPersonas.set(id,p);
	}
	
	
	/*
	 * getAllPersonas
	 * AddPersona
	 * deletePersona
	 * getPersonabyNombre  ===> Lambda
	 * updatePersona
	 * */
	
	
	
	

}
