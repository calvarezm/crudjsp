package cl.Inacap.FormLogin.DTO;

public class Persona {
	//1=> Atributos
	
	private String rut;
	private String nombreApellido;
	private String fechaNacimiento;
	private String emailPersona;
	private String password;
	
	
	
	public Persona(String rut, String nombreApellido, String fechaNacimiento, String emailPersona, String password) {
		super();
		this.rut = rut;
		this.nombreApellido = nombreApellido;
		this.fechaNacimiento = fechaNacimiento;
		this.emailPersona = emailPersona;
		this.password = password;
	}
	
	
	public Persona() {};
	
	
	//2=> Metodos
	
	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getNombreApellido() {
		return nombreApellido;
	}

	public void setNombreApellido(String nombreApellido) {
		this.nombreApellido = nombreApellido;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getEmailPersona() {
		return emailPersona;
	}

	public void setEmailPersona(String emailPersona) {
		this.emailPersona = emailPersona;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
