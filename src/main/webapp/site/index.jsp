<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ page import="cl.Inacap.FormLogin.DTO.Persona" %>  
    
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Responsive</title>
</head>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

<style>
    .border{
       border: 1px solid #000 !important;   
       min-height:20px;
       margin-bottom: 20px;
    }
  
    .col-centered{
        float: none;
        margin: 0 auto;
    }

</style> 
<body>


    <div class="container" style="margin-top: 20px;">

        <div class="row">
            <div class="col-12 text-center">
                <h1>Formulario de persona</h1>
            </div>
        </div>
        
        
        <%
                    
	     int Iden= (int) request.getAttribute("IdentificadorPersona");
	     
	     
	     String sendPost="ControlFormulario.do";
	     if(Iden>-1){
	    	 sendPost="EditPersona.do";
	     }
	     
	     
	     %>
                    
        

        <form action="<%=sendPost %>" method="POST" name="formulario" id="formularioID">
            <div class="row">
            
            	<input type="hidden" name="index" value="${IdentificadorPersona}" />

                <div class="col-md-4 col-centered ">
                    <label for="Nombre">Nombre</label>
                    
                    <% 
                    //Opcion 2 getAttribute	
                    //Persona p= (Persona) request.getAttribute("Persona");
                    		
                    //EL:
                    	//${Persona.nombreApellido}
                    %>
                    <input type="text" class="form-control" name="nombre" id="nombre" value="${Persona.nombreApellido}">
                    <br>    

                    <label for="Rut">Rut</label>
                    <input type="text" class="form-control" name="rut" id="rut" value="${Persona.rut}">

                    <br>

                    <label for="Fecha">Fecha</label>
                    <input type="date" class="form-control" id="fechaNacimiento" name="fechaNacimiento" value="${Persona.fechaNacimiento}">    
            
                
                    <hr>
                    
                    <label for="Email">Email</label>
                    <input type="email" id="Email" name="Email" class="form-control" onchange="validaEmail(this.value)" value="${Persona.emailPersona}">
                    
                    
                    <%
                    
                   
                    
                    
                    String DisplayCampos="";
                    //out.print(Iden);
                    if(Iden>-1){
                    	DisplayCampos="none";
                    }
                    
                    
                    %>
                    
					<div class="" style="display:<%=DisplayCampos%>">
					
	                    <label for="PasswordUser" style="margin-top:20px;">Password</label>
	                    <input type="password"  name="Password" id="PasswordUser" class="form-control">
	
	                    <label for="PasswordUser" style="margin-top:20px;">Re-ingrese Password</label>
	                    <input type="password"  name="Password" id="PasswordValidate" class="form-control">
                    </div>


                    <button class="btn btn-primary btn-block"  type="button" onclick="validarInfo()"  id="btn-send" style="margin-top:20px;">Almacenar información persona</button>

                    
                </div>
            </div>
        </form>
    </div>



    
</body>


<div class="modal fade" id="AvisoMensaje" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="modal-titulo">Modal title</h5> 
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil quo earum saepe. Dolor quod maiores cumque dolore doloribus voluptate iure autem tempora mollitia veritatis esse iste quam temporibus, perspiciatis eaque!
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Aceptar</button>
        </div>
    </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

<script src="site/js/scripts.js"></script>
<script src="site/js/jquery.Rut.js"></script>
<script>
    $(document).ready(function (){
        $('#rut').Rut({
	        on_error: function(){ 

            	alerta("Error en RUT","El RUT ingresado esta mal");
	          
	            $("#btn-send").prop('disabled',true)
	        },
	        on_success: function(){ 
	        	
	            $("#btn-send").prop('disabled',false)
	          
	        },
	        validation: true,
	        format_on: 'keyup'
	    });         


    });

    function validaEmail(Correo){
        if (/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/.test(Correo)){
		    $("#btn-send").prop('disabled',false);
	    }else{
           //alert("Error de ingreso de correo");
           alerta('Error','Error de ingreso de correo');

		   $("#btn-send").prop('disabled',true);
	    }
    }


    function validarInfo(){
    	
        var StringError='';
        var swError=0;
        if($("#nombre").val()===''){
            StringError+='<li>Nombre</li>';
            swError=1;
        }

        if($("#rut").val()===''){
            StringError+='<li>Rut</li>';
            swError=1;
        }

        if($("#fechaNacimiento").val()===''){
            StringError+='<li>Fecha Nacimiento</li>';
            swError=1;
        }

        if($("#Email").val()===''){
            StringError+='<li>Email</li>';
            swError=1;
        }
        
        
        <%
        if(Iden<0){
        %>
        

        if($("#PasswordUser").val()===''){
            StringError+='<li>Password</li>';
            swError=1;
        }

        if($("#PasswordValidate").val()===''){
            StringError+='<li>Password Validate</li>';
            swError=1;
        }
        
        <%
        }
        %>
        

        if(swError==1){
            modalMensaje("Error de datos",'Falta ingresar los siguiente datos <ul>'+StringError+'</ul>');   
            return 0;
        }else{
            if($("#PasswordUser").val()!=$("#PasswordValidate").val()) {
                modalMensaje("Error de Password",'las contraseñas no son las mismas');  
                return 0;
            } else{
            	//document.formulario.submit();
            	$("#formularioID").submit();
            	
                // document.formulario.submit();
                //$("#formularioID").submit();
                /*
                var ObjSend={
                	'nombre':$("#nombre").val(),
                	'rut':$("#rut").val(),
                	'fechaNacimiento':$("#fechaNacimiento").val(),
                	'Email':$("#Email").val(),
                	'PasswordUser':$("#PasswordUser").val(),
                };
                
                $.ajax({
            		type: "POST",
            		data: ObjSend,
            		url: "ControlFormulario.do",
            		success: function(obj){
            			console.log(obj)
            			
            			console.log("asa")
            			alerta("Aviso","Se almacenaron los datos de la persona",link)
            			console.log(link)
            		}
            	});
                
                */
                
                

            }
        }
        



        //modalMensaje("Aviso de modal",'HOLA MUNDO'); 
        

    }

    function modalMensaje(Title,Mensaje){
        $("#modal-titulo").html(Title);
        $(".modal-body").html(Mensaje);


        $('#AvisoMensaje').modal('toggle');
    }

    function alerta(Title,Mensaje,funcion=""){
        
		var Opciones=
		{
            title: Title,
            content: Mensaje,
            icon: 'fa fa-question-circle-o',
            theme: 'supervan',
            closeIcon: true,
            animation: 'scale',
            type: 'orange',
            buttons: {
                cancel: function () {
                	text: 'Aceptar'
                }
            }   
        }		
		
		
        $.confirm(Opciones);

    }


</script>


</html>