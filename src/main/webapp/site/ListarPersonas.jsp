<%@page import="java.util.ArrayList"%>
<%@page import="cl.Inacap.FormLogin.DTO.Persona"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
	List<Persona> arrPersona=new ArrayList<Persona>();


%>

    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lista de personas</title>
</head>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">

<style>
    .border{
       border: 1px solid #000 !important;   
       min-height:20px;
       margin-bottom: 20px;
    }
  
    .col-centered{
        float: none;
        margin: 0 auto;
    }

</style> 
<body>


    <div class="container" style="margin-top: 20px;">

        <div class="row">
            <div class="col-12 text-center">
                <h1>Lista de personas</h1>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-striped" id="myTable">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>RUT</th>
                            <th>Fecha</th>
                            <th>Email</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                            <th>Password Cod.</th>
                        </tr>
                    </thead>

                    <tbody>
                    	<c:forEach items="${ListaPersonas}" var="o" varStatus="ciclo">
                        <tr>
                            <td>${o.nombreApellido }</td>
                            <td>${o.rut }</td>
                            <td>${o.fechaNacimiento }</td>
                            <td>${o.emailPersona }</td>
                            <td class="text-center"><a class="btn btn-sm btn-success" href="EditPersona.do?Iden=${ciclo.index}">Editar persona</a></td>
                            <td class="text-center"><button class="btn btn-sm btn-danger" onclick="deletePersona(${ciclo.index},'Nombre de persona fila')">Elminar persona</button></td>
                        	<td>${o.password }</td>
                        </tr>
                        </c:forEach>
                    </tbody>

                   


                </table>
            </div>


        </div>

        
    </div>



    
</body>


<div class="modal fade" id="AvisoMensaje" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="modal-titulo">Modal title</h5> 
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil quo earum saepe. Dolor quod maiores cumque dolore doloribus voluptate iure autem tempora mollitia veritatis esse iste quam temporibus, perspiciatis eaque!
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Aceptar</button>
        </div>
    </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.0.0.min.js" ></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script> 
<script src="site/js/scripts.js"></script>
<script src="site/js/jquery.Rut.js"></script>
<script>
    $(document).ready(function (){
       
        $('#myTable').DataTable();

    });

    

    function deletePersona(Index,NombrePersona){
        $.confirm({
            title: "Consulta",
            content: "Seguro de eliminar a "+NombrePersona,
            icon: 'fa fa-question-circle-o',
            theme: 'supervan',
            closeIcon: false,
            animation: 'scale',
            type: 'orange',
            buttons: {
                heyThere: {
                    text: 'Si', // text for button
                    btnClass: 'btn-blue', // class for the button
                    action: function(heyThereButton){
                      	
                    	var jsonSend={
                    		'Id':Index
                    	}
                    	
                    	
                    	$.ajax({
                    		type: "POST",
                    		url : "ListartPersonas.do",
                        	data: {"Id":Index,"Opc":1},
                        	success:function (obj){
                        		console.log(obj)		
                        		alert("Se elimino la persona")
                        		setTimeout("location.reload()",4000);
                        		
                        	}
                    			
                    		
                    	})
                    	
                    	
                    }
                },

                cancel: {
                    text: 'No', // text for button
                    btnClass: 'btn-blue', // class for the button
                    action: function(heyThereButton){
                        
                    }
                },
            }   
        });


    }

   

</script>


</html>