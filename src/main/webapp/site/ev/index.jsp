<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<jsp:include page="header.jsp"/>
<section class="container">
    <div class="row">
        <div class="col-md-12 col-12 col-small-12 text-center">
            <b>LOGIN</b>
            <br>
            <input type="Text" value="" name="usuario" placeholder="Usuario">
            <br>
            <br>
            <input type="password" value="" name="contrase�a" placeholder="Contrase�a">
            <br>
            <br>
            <input type="submit" name="usuario.jsp" value="ingresar">
           
            <a href="#">Recuperar clave</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-12 col-small-12 text-center">
            <b>Quienes Somos</b>
            <br>
            Nuestros servicios de calidad y profesionalismo de nuestra gente nos ha permitido dejar a turistas y personas de negocios muy satisfechos en todos los servicios que nuestra empresa ofrece, ofreci�ndoles toda la ayuda y apoyo necesario.
            Ofrecemos todos los servicios con choferes profesionales y experimentados en la zona. Tenemos gu�as que hablan ingl�s, alem�n, franc�s, italiano y portugu�s. Todos los veh�culos son adecuados para recorrer los dif�ciles caminos del desierto de atacama y poseen los implementos necesarios para enfrentar cada expedici�n y aventura. Trabajamos con el mejor personal de la zona avalados por a�os de experiencia en el rubro del turismo en San Pedro de Atacama y con hoteles de alta calidad para que tengan una grata estad�a durante su visita a esta maravillosa zona de Chile.
            Por �stas y muchas otras razones, lo invitamos a conocer �ste espectacular desierto y oasis, y cubrirse de la magica historia de sus pueblos, culturas y artesan�as.    </div>
    </div>
</section>
<jsp:include page="footer.jsp"/>
</html>