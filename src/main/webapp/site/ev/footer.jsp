<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<footer class="container-fluid">
    <div class="row">
        <div class="col-md-12 col-12 col-small-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-4 col-small-4">
            <h1>Contacto</h1>
            <b>Nombre</b>
            <br>
            <textarea name="Nombre usuario">Nombre usuario</textarea>
            <br>
            <b>Mensaje</b>
            <br>
            <textarea name="Mensaje">Nombre usuario</textarea>
            <br>
            <button type="enviar" id="consulta" class="contacto">enviar consulta</button>
        </div>
        
        <div class="col-md-4 col-4 col-small-4">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque culpa numquam, ipsum explicabo omnis, nobis voluptatum maiores laboriosam iure rerum nemo provident. Soluta quas facere aut iusto ducimus asperiores adipisci?
        
        </div>
        <div class="col-md-4 col-4 col-sm-4">
            <h4>exploraciones</h4>
            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/h6niVljJA34" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>        
        </div>
    </div>

</footer>
</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
